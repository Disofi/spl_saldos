﻿using BLL;
using DS_Spl_Saldos.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UTIL.Model;

namespace DS_Spl_Saldos.Controllers
{
    public class LoginController : Controller
    {
        private ControlDisofi control = new ControlDisofi();

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LogOut()
        {
            SessionVariables.SESSION_DATOS_USUARIO = null;
            return RedirectToAction("Login", "Login");
        }

        [HttpPost]
        public JsonResult Login(string _Nombre, string _Contrasena)
        {
            var datosUsuario = new UsuarioModel();
            SessionVariables.SESSION_DATOS_USUARIO = null;
            var validador = 0;
            if (!string.IsNullOrEmpty(_Nombre) && !string.IsNullOrEmpty(_Contrasena))
            {
                datosUsuario.NombreUsuario = _Nombre;
                datosUsuario.Contrasena = _Contrasena;
                var resultadoList = control.login(datosUsuario);
                var resultado = resultadoList == null ? null : resultadoList.Count == 0 ? null : resultadoList[0];

                if (resultado != null)
                {
                    if (resultado.Estado == false)
                    {
                        return Json(new ResponseModel() { Verificador = false, Mensaje = "Usuario deshabilitado" });
                    }
                    else
                    {
                        validador = 1;
                        SessionVariables.SESSION_DATOS_USUARIO = resultado;
                        return Json(new { Validador = validador, DatosUsuario = resultado });
                    }
                }
                else
                {
                    return Json(new ResponseModel() { Verificador = false, Mensaje = "Error de usuario y/o contraseña" });
                }
            }
            else
            {
                return Json(new ResponseModel() { Verificador = false, Mensaje = "Error Datos" });
            }
        }
    }
}
