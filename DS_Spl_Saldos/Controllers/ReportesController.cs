﻿using BLL;
using DS_Spl_Saldos.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UTIL.Model;
using static DS_Spl_Saldos.Util.Autorizacion;

namespace DS_Spl_Saldos.Controllers
{
    public class ReportesController : BaseController
    {
        public ActionResult ReporteSaldo()
        {
            return View();
        }

        [HttpPost]
        [Autorizacion(PERFILES.ADMINISTRADOR)]
        public JsonResult GenerarReporteSaldo (/*List<BdModel> BaseDatos*/string NombreBase,string Anio, string Fecha)
        {
            try
            {
                string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/ArchivosTemporales/");
                //string path = new Util.Excel.EXCEL(controlDisofi()).generarExcelReporteSaldo(mapPath,BaseDatos[0].NombreBase);
                string path = new Util.Excel.EXCEL(controlDisofi()).generarExcelReporteSaldo(mapPath,NombreBase, Anio, Fecha);
                SessionVariables.SESSION_RUTA_ARCHIVO = path;

                return Json(new { Verificador = true });
            }catch(Exception e)
            {
                string error = e.Message.ToString();
                throw (e);
            }
        }
    }
}
