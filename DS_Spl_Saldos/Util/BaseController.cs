﻿using BLL;
using DS_Spl_Saldos.Util.Error;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DS_Spl_Saldos.Util
{
    public class BaseController : Controller
    {
        private ControlDisofi _control = new ControlDisofi();

        public ControlDisofi controlDisofi()
        {
            return _control;
        }

        public ActionResult AbrirError(Errores.ERRORES codigoError, TipoAccionError.TIPO_ACCION_BTN tipoAccionBtn)
        {
            return RedirectToAction("Index", "Error", new { codigoError = codigoError, tipoAccionBtn = tipoAccionBtn });
        }
        public ActionResult AbrirError(Errores.ERRORES codigoError, TipoAccionError.TIPO_ACCION_BTN tipoAccionBtn,
            string controller, string action, string texto)
        {
            return RedirectToAction("Index", "Error", new { codigoError = codigoError, tipoAccionBtn = tipoAccionBtn, controller = controller, action = action, texto = texto });
        }

        public ActionResult DescargarArchivoSaldo(string path, string nombreArchivo = "")
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(path);
            string fileName = new System.IO.FileInfo(path).Name;
            string fileExtension = new System.IO.FileInfo(path).Extension;

            nombreArchivo = nombreArchivo == "" ? fileName : nombreArchivo;

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreArchivo + fileExtension);
        }

        public ActionResult DescargarArchivo()
        {
            string path = SessionVariables.SESSION_RUTA_ARCHIVO;
            return DescargarArchivoSaldo(path, "Reporte Saldos");
        }

    }
}