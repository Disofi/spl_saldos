﻿using DS_Spl_Saldos.Util.Error;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using UTIL.Model;

namespace DS_Spl_Saldos.Util
{
    public class Autorizacion : ActionFilterAttribute, IActionFilter
    {
        private readonly PERFILES[] perfiles;

        public Autorizacion(params PERFILES[] perfiles)
        {
            this.perfiles = perfiles;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string action = filterContext.ActionDescriptor.ActionName;
            string controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;

            UsuarioModel objetoUsuario = SessionVariables.SESSION_DATOS_USUARIO;

            if (objetoUsuario == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                    new RouteValueDictionary(new { controller = "Error", action = "index", codigoError = Errores.ERRORES.ERROR_SESSION_TERMINADA, tipoAccionBtn = TipoAccionError.TIPO_ACCION_BTN.IR_LOGIN })
                    );
                filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
            }
            else
            {
                if (perfiles.Where(m => Convert.ToInt32(m).Equals(objetoUsuario.IdPerfil)).Count() == 0)
                {
                    filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary(new { controller = "Error", action = "Index", codigoError = Errores.ERRORES.ERROR_NO_AUTORIZADO, tipoAccionBtn = TipoAccionError.TIPO_ACCION_BTN.IR_LOGIN })
                        );
                    filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
                }
            }
            base.OnActionExecuting(filterContext);
        }

        public enum PERFILES
        {
            ADMINISTRADOR = 1
        }
    }
}