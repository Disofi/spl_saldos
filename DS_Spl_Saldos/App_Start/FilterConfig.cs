﻿using System.Web;
using System.Web.Mvc;

namespace DS_Spl_Saldos
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
