﻿$(document).ready(function () {
    try {
        $('#dataTable').DataTable({
            language: {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ Registros",
                "sZeroRecords": "&nbsp;",
                "sEmptyTable": "&nbsp;",
                "sInfo": "Encontrados: _TOTAL_ Registros (Mostrando del _START_ al _END_)",
                "sInfoEmpty": "* No se han encontrado resultados en la búsqueda",
                "sInfoFiltered": "",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            aoColumnDefs: [{ 'bSortable': false, 'aTargets': ['no-sortable'] }]
        });
    }
    catch (e) { }
    /*
    $("#iframe").fancybox({
        width: '85%',
        height: '60%',
        autoScale: false,
        transitionIn: 'elastic',
        transitionOut: 'elastic',
        type: 'iframe'
    });
    */

    $("#modalLayoutEmpresasAgregar").click(function () {
        var empresa = $("#ddlLayoutEmpresas").val();

        var urlSeleccionaEmpresa = $("#urlSeleccionaEmpresa").val();

        $.ajax({
            url: urlSeleccionaEmpresa,
            type: "POST",
            data: { _IdEmpresa: empresa },
            async: true,
            success: function (data) {
                var urlHome = $("#urlHome").val();
                window.location = urlHome;
            }
        });
    });
});
function primeraLetraMayuscula(cadena) {
    return cadena.charAt(0).toUpperCase() + cadena.slice(1);
}

$(document).ready(function () {
    $("#aBaseModalCambioContrasena").click(function () {
        $("#btnBaseModalCambioContrasena").click();
    });

    $("#baseBtnCambiaContrasena").click(function () {
        console.log("sdlfhiosdbvnkld");
        $.ajax({
            url: "BaseCambiaContrasena",
            type: "POST",
            data: {
                contrasena: $("#baseTxtContrasena").val(),
            },
            async: true,
            success: function (data) {
                console.log(data);
                if (data.Verificador) {
                    $("#baseBtnCerrarModal").click();
                    abrirInformacion("Cambio Contraseña", data.Mensaje);
                    $("#baseTxtContrasena").val("");
                }
                else {
                    abrirError("Cambio Contraseña", data.Mensaje);
                }
            }
        });
    });
});

function abrirLoadingCompleto(texto) {
    /*
    if (texto === undefined || texto === null) {
        texto = "Cargando";
    }
    $("#loading-ajax-texto").html(texto);
    jQuery("#loading-ajax").show();
    */
}
function cerrarLoadingCompleto() {
    /*
    jQuery("#loading-ajax").hide();
    */
}




var tipoAlert = 1;

function abrirError(titulo, mensaje, callBackOK) {
    callBackOK = (callBackOK === undefined || callBackOK === null) ? function () { } : callBackOK;
    if (tipoAlert === 1) {
        swal({
            title: titulo,
            text: mensaje,
            icon: 'error',
            buttons: {
                confirm: {
                    text: 'Aceptar',
                    value: true,
                    visible: true,
                    className: 'btn btn-danger',
                    closeModal: true
                }
            }
        }).then((value) => {
            if (value === true) {
                callBackOK();
            }
        });
    }
    else {
        alert(mensaje);
    }
}

function abrirAprobacion(titulo, mensaje, callBackOK) {
    callBackOK = (callBackOK === undefined || callBackOK === null) ? function () { } : callBackOK;
    if (tipoAlert === 1) {
        swal({
            title: titulo,
            text: mensaje,
            icon: 'success',
            buttons: {
                confirm: {
                    text: 'Aceptar',
                    value: true,
                    visible: true,
                    className: 'btn btn-success',
                    closeModal: true
                }
            }
        }).then((value) => {
            if (value === true) {
                callBackOK();
            }
        });
    }
    else {
        alert(mensaje);
    }
}

function abrirInformacion(titulo, mensaje, callBackOK) {
    callBackOK = (callBackOK === undefined || callBackOK === null) ? function () { } : callBackOK;
    if (tipoAlert === 1) {
        swal({
            title: titulo,
            text: mensaje,
            icon: 'info',
            buttons: {
                confirm: {
                    text: 'Aceptar',
                    value: true,
                    visible: true,
                    className: 'btn btn-info',
                    closeModal: true
                }
            }
        }).then((value) => {
            if (value === true) {
                callBackOK();
            }
        });
    }
    else {
        alert(mensaje);
    }
}

function abrirConfirmacion(titulo, mensaje, callBackOK, callBackCancel, esError) {
    callBackOK = (callBackOK === undefined || callBackOK === null) ? function () { } : callBackOK;
    callBackCancel = (callBackCancel === undefined || callBackCancel === null) ? function () { } : callBackCancel;
    if (tipoAlert === 1) {
        swal({
            title: titulo,
            text: mensaje,
            icon: (esError ? 'error' : 'info'),
            buttons: {
                cancel: {
                    text: 'Cancelar',
                    value: null,
                    visible: true,
                    className: 'btn btn-default',
                    closeModal: true,
                },
                confirm: {
                    text: 'Confirmar',
                    value: true,
                    visible: true,
                    className: 'btn btn-success',
                    closeModal: true
                }
            }
        }).then((value) => {
            if (value === true) {
                callBackOK();
            }
            else {
                callBackCancel();
            }
        });
    }
    else {
        var conf = confirm(mensaje);

        if (conf) {
            if (callBackOK !== null && callBackOK !== undefined && (callBackOK instanceof Function)) {
                callBackOK();
            }
        }
        else {
            if (callBackCancel !== null && callBackCancel !== undefined && (callBackCancel instanceof Function)) {
                callBackCancel();
            }
        }
    }
}
function llamadaAjax(_url, _tipo, _data, _success, _error, _opciones) {
    if (_opciones === undefined || _opciones === null) {
        _opciones = {};
    }

    $.ajax({
        url: _url,
        type: _tipo,
        data: _data,
        success: function (response) {
            _success(response);
        },
        error: function (error) {
            if (_error === null || _error === undefined) {
                var mensaje = "";

                if (error.status === 404) {
                    mensaje = "Ruta no encontrada";
                }
                if (error.status === 500) {
                    mensaje = "Error de servidor";
                }

                abrirError("Error", mensaje);
            }
            else {
                _error(error);
            }
        },
        async: _opciones.asincronico === undefined ? true : _opciones.asincronico
    });
}
var llamadaAjaxEnum = {
    POST: "POST",
    GET: "GET"
}




var spinnerButton = function () {
    return "<span class='spinner-border spinner-border-sm' role='status' aria-hidden='true'></span> ";
}


var ____botonesLoading = [];

var addBotonLoading = function (id) {
    if (____botonesLoading.find(m => m.id === id) === undefined) {
        ____botonesLoading.push({
            id: id,
            htmlOriginal: $("#" + id).html(),
            htmlLoading: (spinnerButton() + $("#" + id).html())
        });
    }
}

function activarLoadingBoton(id) {
    addBotonLoading(id);
    $("#" + id).html(____botonesLoading.find(m => m.id === id).htmlLoading);
    $("#" + id).attr("disabled", "disabled");
}

function desactivarLoadingBoton(id) {
    addBotonLoading(id);
    $("#" + id).html(____botonesLoading.find(m => m.id === id).htmlOriginal);
    $("#" + id).removeAttr("disabled");
}





function esEmail(email) {
    if (email.length > 0) {
        return expresion = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    }
    else {
        return true;
    }
}