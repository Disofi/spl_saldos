﻿$(document).ready(function () {

    $("#ddlBaseDatos").select2({ placeholder: "Seleccione Empresa" });

    $("#btnCrearReporte").click(function () {
        $("#divCreandoReporte").fadeOut();
        $("#divCreandoReporteCargando").fadeIn();
        $("#divCreandoReporteCreado").fadeOut();
        $("#divCreandoReporteError").fadeOut();
        $("#divTablaStock").fadeOut();
        
        var Base = $("#ddlBaseDatos").val();
        var fecha = $("#txtFecha").val();
        fecha = fecha.replace("-", "");
        fecha = fecha.replace("-", "");
        var anio = fecha.substr(0,4);
        if (Base != "" && fecha != "") {
            $("#divCreandoReporte").fadeIn();
            $.ajax({
                type: "POST",
                url: "GenerarReporteSaldo",
                data: {
                    NombreBase: Base
                    ,Anio: anio
                    ,Fecha: fecha
                },
                async: true,
                success: function (response) {
                    if (response.Verificador) {
                        $("#divCreandoReporteCargando").fadeOut("", function () {
                            $("#divCreandoReporteCreado").fadeIn();
                        });
                    }
                },
                error: function (a, b, c) {
                    $("#divCreandoReporteError").fadeIn();
                }
            });
        } else {
            abrirError("Usuario", "Debe ingresar Base de datos y/o Fecha");
        }
    });
    $("#divCreandoReporteCreadoDescargar").click(function () {
        $("#divCreandoReporte").fadeOut();
        $("#divCreandoReporteCargando").fadeIn();
        $("#divCreandoReporteCreado").fadeOut();
        $("#divCreandoReporteError").fadeOut();
        window.open("DescargarArchivo");
    });

    

});

var vendedoresTodos = [];