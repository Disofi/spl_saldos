﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTIL;
using UTIL.Model;

namespace BLL
{
    public class FactoryAcceso
    {
        #region Login y Menu

        public List<MenuModel> MenuUsuario(int idPerfil)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_Menu", new System.Collections.Hashtable()
                {
                    {"idPerfil", idPerfil }
                });
                return UTIL.Mapper.BindDataList<MenuModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<UsuarioModel> login(UsuarioModel usuario)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_Login", new System.Collections.Hashtable()
                {
                    {"pv_Usuario", usuario.NombreUsuario },
                    {"pv_Contrasena", usuario.Contrasena },
                    {"pv_ContrasenaMD5", HashMd5.GetMD5(usuario.Contrasena) }
                });
                return UTIL.Mapper.BindDataList<UsuarioModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        #endregion

        #region GeneraExcel

        public DataTable obtenerExcelSaldo(string bd, string Anio, string Fecha)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("reporteSaldos", new System.Collections.Hashtable()
                {
                    {"baseDatos",bd },
                    {"FechaPeriodo",Anio },
                    {"FContabiliza",Fecha }
                });
                return data;
            }
            catch(Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        #endregion
    }
}
