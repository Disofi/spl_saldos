﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTIL.Model;

namespace BLL
{
    public class ControlDisofi
    {
        private FactoryAcceso _Control = new FactoryAcceso();

        #region Login y Menu

        public List<MenuModel> MenuUsuario(int idPerfil)
        {
            return _Control.MenuUsuario(idPerfil);
        }
        public List<UsuarioModel> login(UsuarioModel usuario)
        {
            return _Control.login(usuario);
        }

        #endregion

        #region GeneraExcel

        public DataTable obtenerExcelSaldo(string bd, string Anio, string Fecha)
        {
            return _Control.obtenerExcelSaldo(bd,Anio,Fecha);
        }

        #endregion
    }
}
