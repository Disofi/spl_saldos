﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Model
{
    public class MenuModel
    {
        public int IdMenu { get; set; }
        public string Clase { get; set; }
        public string PieMenu { get; set; }
        public string Titulo { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public string IdPerfil { get; set; }
        public int Estado { get; set; }
        public int Orden { get; set; }
    }
}
