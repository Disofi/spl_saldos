﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Model
{
    public class UsuarioModel
    {
        public int IdUsuario { get; set; }
        public string Contrasena { get; set; }
        public int IdPerfil { get; set; }
        public bool? Estado { get; set; }
        public string Perfil { get; set; }
        public string NombreUsuario { get; set; }
        public string NombreCompleto { get; set; }
        public string EstadoString { get { return Estado == null ? "" : (bool)Estado ? "SI" : "NO"; } }
    }
}
