﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Model
{
    public class ResponseModel
    {
        public bool Verificador { get; set; }
        public string Mensaje { get; set; }
        public int Id { get; set; }
        public string Url { get; set; }
        public string RutaArchivo { get; set; }
    }
}
