USE [master]
GO
/****** Object:  Database [DSSALDOS]    Script Date: 11-09-2020 18:32:19 ******/
CREATE DATABASE [DSSALDOS]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DSSALDOS', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\DSSALDOS.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'DSSALDOS_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\DSSALDOS_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [DSSALDOS] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DSSALDOS].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DSSALDOS] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DSSALDOS] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DSSALDOS] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DSSALDOS] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DSSALDOS] SET ARITHABORT OFF 
GO
ALTER DATABASE [DSSALDOS] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DSSALDOS] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DSSALDOS] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DSSALDOS] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DSSALDOS] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DSSALDOS] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DSSALDOS] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DSSALDOS] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DSSALDOS] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DSSALDOS] SET  ENABLE_BROKER 
GO
ALTER DATABASE [DSSALDOS] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DSSALDOS] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DSSALDOS] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DSSALDOS] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DSSALDOS] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DSSALDOS] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DSSALDOS] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DSSALDOS] SET RECOVERY FULL 
GO
ALTER DATABASE [DSSALDOS] SET  MULTI_USER 
GO
ALTER DATABASE [DSSALDOS] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DSSALDOS] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DSSALDOS] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DSSALDOS] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [DSSALDOS] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'DSSALDOS', N'ON'
GO
ALTER DATABASE [DSSALDOS] SET QUERY_STORE = OFF
GO
USE [DSSALDOS]
GO
/****** Object:  Table [dbo].[DS_TipoUsuario]    Script Date: 11-09-2020 18:32:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_TipoUsuario](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NULL,
	[UrlInicio] [varchar](1000) NULL,
	[Estado] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_Usuarios]    Script Date: 11-09-2020 18:32:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_Usuarios](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NombreUsuario] [varchar](100) NULL,
	[NombreCompleto] [varchar](100) NULL,
	[Contrasena] [varchar](1000) NULL,
	[CodigoUsuario] [varchar](100) NULL,
	[Email] [varchar](1000) NULL,
	[IdTipo] [int] NULL,
	[CentroCosto] [varchar](100) NULL,
	[Estado] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Menu]    Script Date: 11-09-2020 18:32:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menu](
	[Id_Menu] [int] IDENTITY(1,1) NOT NULL,
	[Clase] [varchar](50) NULL,
	[PieMenu] [varchar](50) NULL,
	[Titulo] [varchar](200) NULL,
	[Action] [varchar](200) NULL,
	[Controler] [varchar](200) NULL,
	[Id_Perfil] [int] NULL,
	[Activo] [int] NULL,
	[Orden] [int] NULL,
	[IdEmpresa] [int] NULL,
 CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED 
(
	[Id_Menu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DS_TipoUsuario] ON 

INSERT [dbo].[DS_TipoUsuario] ([Id], [Nombre], [UrlInicio], [Estado]) VALUES (1, N'Administrador', N'', 1)
SET IDENTITY_INSERT [dbo].[DS_TipoUsuario] OFF
SET IDENTITY_INSERT [dbo].[DS_Usuarios] ON 

INSERT [dbo].[DS_Usuarios] ([Id], [NombreUsuario], [NombreCompleto], [Contrasena], [CodigoUsuario], [Email], [IdTipo], [CentroCosto], [Estado]) VALUES (1, N'admin', N'Administrador', N'81dc9bdb52d04dc20036dbd8313ed055', NULL, NULL, 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[DS_Usuarios] OFF
SET IDENTITY_INSERT [dbo].[Menu] ON 

INSERT [dbo].[Menu] ([Id_Menu], [Clase], [PieMenu], [Titulo], [Action], [Controler], [Id_Perfil], [Activo], [Orden], [IdEmpresa]) VALUES (1, N'fa fa-home', N'Reportes', N'Resporte Saldo', N'ReporteSaldo', N'Reportes', 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Menu] OFF
/****** Object:  StoredProcedure [dbo].[reporteSaldos]    Script Date: 11-09-2020 18:32:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[reporteSaldos]
@baseDatos as varchar(100),
@FechaPeriodo as varchar(10),
@FContabiliza as varchar(10)
as
begin
	set nocount on       
	DECLARE	@query nvarchar(max)
	
	select	@query = ''
	begin try
		select	@query = @query + '
			select
			cwpctas.pccodi as Cuenta
			,cwpctas.pcdesc as NombreCuenta
			,cwtauxi.codaux as CodAux
			,cwtauxi.RutAux as RutAux
			,cwtauxi.nomaux as Nombre
			,(select top(1)CpbNum from ['+@baseDatos+'].softland.cwmovim 
									where MovNumDocRef=s.MovNumDocRef 
									and MovTipDocRef=s.MovTipDocRef
									and PctCod=s.PctCod
									and CodAux=s.CodAux
									order by CpbFec) as NumComprobante
			,(select top(1)MovGlosa from ['+@baseDatos+'].softland.cwmovim 
									where MovNumDocRef=s.MovNumDocRef 
									and MovTipDocRef=s.MovTipDocRef
									and PctCod=s.PctCod
									and CodAux=s.CodAux
									order by CpbFec) as Glosa
			,convert(varchar,(select top(1)cpbfec from ['+@baseDatos+'].softland.cwmovim 
									where MovNumDocRef=s.MovNumDocRef 
									and MovTipDocRef=s.MovTipDocRef
									and PctCod=s.PctCod
									and CodAux=s.CodAux
									order by CpbFec),105) as FechaContable
			,convert(varchar,(select top(1)MovFe from ['+@baseDatos+'].softland.cwmovim 
									where MovNumDocRef=s.MovNumDocRef 
									and MovTipDocRef=s.MovTipDocRef
									and PctCod=s.PctCod
									and CodAux=s.CodAux
									order by CpbFec),105) as FechaEmision
			,convert(varchar,(select top(1)MovFv from ['+@baseDatos+'].softland.cwmovim 
									where MovNumDocRef=s.MovNumDocRef 
									and MovTipDocRef=s.MovTipDocRef
									and PctCod=s.PctCod
									and CodAux=s.CodAux
									order by CpbFec),105) as FechaVencimiento
			,case when (DATEDIFF(day,(select top(1)MovFv from ['+@baseDatos+'].softland.cwmovim 
									where MovNumDocRef=s.MovNumDocRef 
									and MovTipDocRef=s.MovTipDocRef
									and PctCod=s.PctCod
									and CodAux=s.CodAux
									order by CpbFec),''' + @FContabiliza + ''')) < 0
				then 0 else (DATEDIFF(day,(select top(1)MovFv from ['+@baseDatos+'].softland.cwmovim 
									where MovNumDocRef=s.MovNumDocRef 
									and MovTipDocRef=s.MovTipDocRef
									and PctCod=s.PctCod
									and CodAux=s.CodAux
									order by CpbFec),''' + @FContabiliza + ''')) end
			 as DiasDeAtraso
			,(select top(1)TtdCod from ['+@baseDatos+'].softland.cwmovim 
									where MovNumDocRef=s.MovNumDocRef 
									and MovTipDocRef=s.MovTipDocRef
									and PctCod=s.PctCod
									and CodAux=s.CodAux
									order by CpbFec) as Tipo
			,s.movnumdocref as NumDoc
			,s.saldo as Pesos
			,s.saldo /  (case when (select top(1)MovEquiv from ['+@baseDatos+'].softland.cwmovim where MovNumDocRef=s.MovNumDocRef 
									and TtdCod=s.MovTipDocRef and	numdoc=s.MovNumDocRef
									order by CpbFec) 
									<= 0 
									then 1 
									else (select top(1)MovEquiv from ['+@baseDatos+'].softland.cwmovim where MovNumDocRef=s.MovNumDocRef 
									and TtdCod=s.MovTipDocRef and	numdoc=s.MovNumDocRef
									order by CpbFec) end) as Dolares
			,(select top(1)MovEquiv from ['+@baseDatos+'].softland.cwmovim where MovNumDocRef=s.MovNumDocRef 
									and TtdCod=s.MovTipDocRef and	numdoc=s.MovNumDocRef
									order by CpbFec) as TipoCambio
			,isnull(vend.VenDes,'''') AS Vendedor
			,isnull((select top (1) NomCon from ['+@baseDatos+'].softland.cwtaxco where carcon = ''0003'' and CodAuc = s.CodAux),'''') as Contacto
			,isnull((select top (1) FonCon from ['+@baseDatos+'].softland.cwtaxco where carcon = ''0003'' and CodAuc = s.CodAux),'''') as NºFonoContacto
			,isnull((select top (1) Email from ['+@baseDatos+'].softland.cwtaxco where carcon = ''0003'' and CodAuc = s.CodAux),'''') as CorreoContacto
		FROM (select MovNumDocRef, MovTipDocRef,PctCod,max(VendCod) as VendCod, CodAux, max(movdebe) as debe, max(movhaber) as haber, sum(movdebe-movhaber) as saldo 
									from ['+@baseDatos+'].softland.cwmovim
									inner join ['+@baseDatos+'].softland.cwcpbte on cwmovim.CpbNum=cwcpbte.CpbNum
												and cwmovim.CpbAno=cwcpbte.CpbAno
									where cwcpbte.cpbest=''V''
									and (cwcpbte.CpbFec <= '''+@FContabiliza+''') 
									and cwmovim.CpbAno='''+@FechaPeriodo+'''
									and cwmovim.MovNumDocRef<>''00''
									group by MovNumDocRef, MovTipDocRef,PctCod, CodAux
									having sum(movdebe-movhaber)<>0
									UNION ALL
									select MovNumDocRef, MovTipDocRef,PctCod,max(VendCod) as VendCod, CodAux, max(movdebe) as debe, max(movhaber) as haber, sum(movdebe-movhaber) as saldo 
									from ['+@baseDatos+'].softland.cwmovim
									inner join ['+@baseDatos+'].softland.cwcpbte on cwmovim.CpbNum=cwcpbte.CpbNum
												and cwmovim.CpbAno=cwcpbte.CpbAno
									where cwcpbte.cpbest=''V''
									and (cwcpbte.CpbFec <= '''+@FContabiliza+''') 
									and cwmovim.CpbAno='''+@FechaPeriodo+'''
									and cwmovim.MovNumDocRef=''00''
									and cwmovim.codaux<>''0000000000''
									group by MovNumDocRef, MovTipDocRef,PctCod, CodAux
									having sum(movdebe-movhaber)<>0
		) s
		left join ['+@baseDatos+'].softland.cwttdoc cwttdoc on s.movtipdocref=cwttdoc.coddoc 
		left join ['+@baseDatos+'].softland.cwpctas cwpctas on s.pctcod=cwpctas.pccodi
		left join ['+@baseDatos+'].softland.cwtauxi cwtauxi on s.codaux=cwtauxi.codaux
		left JOIN ['+@baseDatos+'].softland.cwtvend vend ON s.VendCod = vend.VenCod
		Group By cwpctas.pccodi,cwpctas.pcdesc,cwtauxi.codaux,cwtauxi.RutAux
					,s.movnumdocref, s.MovTipDocRef,s.PctCod, cwtauxi.nomaux,cwttdoc.desdoc,cwpctas.PCAUXI
					,cwpctas.PCCDOC,cwttdoc.coddoc, vend.VenDes,s.CodAux, s.saldo

		'
		exec( @query)
		--print @query
	end try
	begin catch
		print (error_message())
	end catch
	
	set nocount OFF       
end  

--exec reporteSaldos 'SPL','2020','20200710'
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_Menu]    Script Date: 11-09-2020 18:32:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_GET_Menu]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[SP_GET_Menu]
@IdPerfil INT
as  
SELECT m.Id_Menu,m.Clase,m.PieMenu,m.Titulo,m.[Action],Controller = m.Controler
from Menu m 
where m.Id_Perfil = @IdPerfil and m.Activo = 1 order by m.Orden 
GO
/****** Object:  StoredProcedure [dbo].[SP_Login]    Script Date: 11-09-2020 18:32:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_Login]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE procedure [dbo].[SP_Login]  
	@pv_Usuario varchar(100)
,	@pv_Contrasena varchar(100)
,	@pv_ContrasenaMD5 varchar(100)
as 
begin
	set nocount on       
	DECLARE	@query nvarchar(max)
	
	select	@query = ''
	begin try
		select	@query = @query + '
		SELECT	TOP 1 
				Id = usr.Id
		,		NombreUsuario = usr.NombreUsuario
		,		Nombres = usr.NombreCompleto
		,		tipoUsuario = tipo.Nombre
		,		Email = usr.email
		,		IdPerfil = usr.IdTipo
		,		CentroCosto = usr.CentroCosto
		,		Estado = usr.Estado
		,		Contrasena = usr.Contrasena
		FROM	[DS_Usuarios] AS usr
			LEFT JOIN [DS_TipoUsuario] AS tipo ON usr.IdTipo=tipo.Id
			WHERE usr.NombreUsuario=''' + @pv_Usuario + ''' AND usr.Contrasena = ''' + @pv_ContrasenaMD5 + '''
		'
		exec( @query)
	end try
	begin catch
		print (error_message())
	end catch
	
	set nocount OFF       
end  
GO
USE [master]
GO
ALTER DATABASE [DSSALDOS] SET  READ_WRITE 
GO
